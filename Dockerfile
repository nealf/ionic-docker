FROM node:8

RUN npm install -g ionic cordova

WORKDIR /srv

ENTRYPOINT ["/bin/bash", "-c"]
